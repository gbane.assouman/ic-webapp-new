FROM python:3.6-alpine
LABEL maintainer "GBANE Assouman gbane.assouman@gmail.com"
WORKDIR /opt
RUN pip install flask==1.1.2
COPY . .
ENV ODOO_URL="https://odoo.com"
ENV PGADMIN_URL="https://pgadmin.org"
EXPOSE 8080
ENTRYPOINT [ "python","app.py" ]